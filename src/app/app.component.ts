import { Component } from '@angular/core';
import * as PouchDB from 'pouchdb'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  // public userDB = new PouchDB('users');
}

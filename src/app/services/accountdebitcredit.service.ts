import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class AccountDebitCreditService {
  
  authenticatedUser: any = [];
  public accountDebitCreditDB = new PouchDB('steelaccountdebitcredit');
  

  constructor(private _router: Router){}

  addDebitCredit(account){
    var self = this;
    console.log("ACCOUNT FOR DEBIT CREDIT:-----", account)
    return self.accountDebitCreditDB.put(account)
  }
  
  getDebitCredits(account){
    var self = this;
    return self.accountDebitCreditDB.createIndex({
      index: {
        fields: ['accountid']
      }
    }).then(function(){
      return self.accountDebitCreditDB.find({
        selector: {
          accountid:{
            $eq:account._id
          }
        }
      });
    })
  }

}
import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import {ElectronService} from 'ngx-electron'
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

export interface UserData {
  purchasenumber ? : string;
  createdby ? : string;
  purchasecustomer ? : string;
  productcount ? : number;
  purchasenotes ? : string;
  purchasedate ? : string;
  challanno ? : string;
  billno ? : string;
  purchasetotal ? : number;
  othercharges ? : number;
  createdat ? : string;
  _id ? : string;
  _rev ? : string;
}

@Component({
    selector: 'purchase',
    providers: [LoginService, ProductService, InvoiceService, PurchaseService, ElectronService],
    templateUrl: 'purchase.html',
    styleUrls: ['purchase.css']
})

export class purchase {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  public purchaseDB = new PouchDB('steelcustomerpurchase');
  public devPurchaseDB = new PouchDB('devsteelpurchase');
  public localPurchasesDB = new PouchDB('http://localhost:5984/localsteelpurchases');
  public ibmPurchasesDB = new PouchDB('https://dewansteel.cloudant.com/dewanpurchases', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });
  public cloudantPurchasesDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewanpurchases', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });

  dateFilterForm: FormGroup;
  
  allPurchases: any = [];
  purchasesTotal: any = null;

  displayedColumns = [
    "createdat",
    "challanno",
    "billno",
    'purchasenumber', 
    'createdby', 
    'purchasecustomer', 
    'productcount', 
    "purchasenotes",
    "purchasetotal"
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  

  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);

  toDate: any = null
  fromDate: any = null
  authUser: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService,
              private _purchaseService: PurchaseService, 
              private _formBuilder: FormBuilder, 
              private _electronService: ElectronService, 
              private _router: Router) {
    this.loadDataSource()
    this._buildAddProductForm()
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }

  private _buildAddProductForm(){
    this.dateFilterForm = this._formBuilder.group({
      fromdate: [''],
      todate: ['']
    })
  }

  ngOnInit(){
    var self = this
    self.syncDb()
  }

  syncDb(){
    var self = this
    // var sync = PouchDB.sync(self.purchaseDB, self.ibmPurchasesDB, {
    //   live: true,
    //   retry: true
    // }).on('change', function (info) {
    //   console.log("CHANGE EVENT", info)
    //   if (info.direction === 'pull'){
    //     self.loadDataSource()
    //   }
    //   // handle change
    // }).on('paused', function (err) {
    //   // replication paused (e.g. replication up to date, user went offline)
    //   console.log("PAUSE EVENT FROM PURCHASE", err)
    // }).on('active', function () {
    //   // replicate resumed (e.g. new changes replicating, user went back online)
    //   console.log("ACTIVE ACTIVE !!")
    // }).on('denied', function (err) {
    //   // a document failed to replicate (e.g. due to permissions)
    //   console.log("DENIED DENIED !!", err)
    // }).on('complete', function (info) {
    //   // handle complete
    //   console.log("COMPLETED !!", info)
    // }).on('error', function (err) {
    //   // handle error
    //   console.log("ERROR ERROR !!", err)
    // });
    var opts = { live: true, retry: true };
    self.purchaseDB.replicate.from(self.ibmPurchasesDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM ONE-WAY PURCHASES REPLICATION:--", info)
      self.purchaseDB.sync(self.ibmPurchasesDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY PURCHASES SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          self.loadDataSource()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC PURCHASES", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY PURCHASES SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    })
  }

  cloudantSync(){
    var self = this
    var opts = { live: true, retry: true };
    self.purchaseDB.replicate.to(self.cloudantPurchasesDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY PURCHASES REPLICATION:--", info)
      // self.productsDB.sync(self.cloudantProductsDB, opts).on('change', function (info) {
      //   console.log("CHANGE EVENT FROM TWO-WAY CLOUDANT PRODUCTS SYNC:--", info)
      //   if (info.direction === 'pull' || info.direction === 'push'){
      //     self.loadSourceData()
      //   }
      // }).on('paused', function (err) {
      //   console.log("PAUSE EVENT FROM TWO-WAY SYNC CLOUDANT PRODUCTS", err)
      // }).on('active', function () {
      //   console.log("ACTIVE ACTIVE FROM TWO-WAY PRODUCTS CLOUDANT SYNC!!")
      // }).on('denied', function (err) {
      //   console.log("DENIED DENIED !!", err)
      // }).on('complete', function (info) {
      //   console.log("COMPLETED !!", info)
      // }).on('error', function (err) {
      //   console.log("ERROR ERROR FROM SYNC CLOUDANT PRODUCTS!!", err)
      // })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT PURCHASES!!", err)
    })
  }

  goToPrint(){
    // this._router.navigate(['printpurchases'])
    this._electronService.ipcRenderer.send('PurchasesPrint')
  }

  loadDataSource(){
    var self = this;
    self._purchaseService.allPurchases().then(function(result){
      const users: UserData[] = [];
      self.allPurchases = [];
      result.rows.map(function(row){
        users.push(createNewUser(row.doc))
        self.allPurchases.push(row.doc); 
      })
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
      self.getPurchasesTotal(self.allPurchases)
    }).catch(function(err){
      console.log(err);
    })

  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    console.log("EVENT ROM DATE CHANGE:----", event.value)
    // this.events.push(`${type}: ${event.value}`);
    if (type === 'input' || type === 'change'){
      this.fromDate = event.value.toISOString()
    }
    if (type === 'input2' || type === 'change2'){
      this.toDate = event.value.toISOString()
    }
    console.log("TO DATE:----\n", this.toDate, "\n", "FROM DATE:----\n", this.fromDate)

    if (this.fromDate < this.toDate){
      console.log("FROM DATE  SMALL")
    }else{
      console.log("FROM DATE LARGER")
    }
    if (this.toDate < this.fromDate){
      console.log("TO DATE  SMALL")
    }else{
      console.log("TO DATE LARGER")
    }

    if (this.toDate != null && this.fromDate != null){

      var self = this
      self._purchaseService.filterPurchases(self.toDate, self.fromDate).then(function(result){
        console.log("RESULT FROM FILTER DEBIT CREDIT:--", result)
        // self.allInvoices = []
        // result.docs.forEach(function(doc){
        //   self.allInvoices.push(doc)
        // })
        const users: UserData[] = [];
        self.allPurchases = [];
        result.docs.forEach(function(row){
          users.push(createNewUser(row))
          self.allPurchases.push(row)
        })
        self.dataSource = new MatTableDataSource(users);
        self.dataSource.paginator = self.paginator;
        self.dataSource.sort = self.sort;
        self.getPurchasesTotal(self.allPurchases)
      }).catch(function(err){
        console.log(err)
      })
    }

  }


  clearFilters(){
    (<FormGroup>this.dateFilterForm).setValue({'fromdate':'', 'todate':''}, {onlySelf: true})
    this.loadDataSource()
    this.toDate = null
    this.fromDate = null
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  getPurchasesTotal(allPurchases){
    var sum = 0
    for (let doc of allPurchases){
      sum = sum + doc.purchasetotal
    }
    this.purchasesTotal = sum
    // this.invoicesTotal = Number(this.invoicesTotal)
    console.log("SUM FROM PURCHASES TOTAL:===", sum, this.purchasesTotal)
  }

  getSinglePurchase(purchase){
    console.log("Selected Purchase:===", purchase)
    this._router.navigate(['/purchase', purchase._id])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }  

  logout(){
    this._loginService.logout()
  }  
}


function createNewUser(row): UserData {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
  console.log("ROW TO SHOW FOR PAYMENTS", row)
  // if (row.purchasenotes){

  //   if (row.billno && row.challanno){

  //     return {
  //       "_id": row._id,
  //       "_rev": row._rev,
  //       "purchasecustomer" : row.purchasecustomer.name.toString(),
  //       "createdat" : row.createdat.toString(),
  //       "createdby" : row.createdby._id.toString(),
  //       "purchasenotes" : row.purchasenotes.toString(),
  //       "purchasenumber" : row.purchasenumber.toString(),
  //       "purchasetotal" : row.purchasetotal,
  //       "billno" : row.billno,
  //       "challanno" : row.challanno,
  //       "productcount" : row.purchaseproducts.length
  //     };
  //   }
  //   if (!row.billno && row.challanno){

  //     return {
  //       "_id": row._id,
  //       "_rev": row._rev,
  //       "purchasecustomer" : row.purchasecustomer.name.toString(),
  //       "createdat" : row.createdat.toString(),
  //       "createdby" : row.createdby._id.toString(),
  //       "purchasenotes" : row.purchasenotes.toString(),
  //       "purchasenumber" : row.purchasenumber.toString(),
  //       "purchasetotal" : row.purchasetotal,
  //       "billno" : null,
  //       "challanno" : row.challanno,
  //       "productcount" : row.purchaseproducts.length
  //     };

  //   }
  //   if (row.billno && !row.challanno){
  //     return {
  //       "_id": row._id,
  //       "_rev": row._rev,
  //       "purchasecustomer" : row.purchasecustomer.name.toString(),
  //       "createdat" : row.createdat.toString(),
  //       "createdby" : row.createdby._id.toString(),
  //       "purchasenotes" : row.purchasenotes.toString(),
  //       "purchasenumber" : row.purchasenumber.toString(),
  //       "purchasetotal" : row.purchasetotal,
  //       "billno" : row.billno,
  //       "challanno" : null,
  //       "productcount" : row.purchaseproducts.length
  //     };
  //   }

  //   if (!row.billno && !row.challanno){
  //     return {
  //       "_id": row._id,
  //       "_rev": row._rev,
  //       "purchasecustomer" : row.purchasecustomer.name.toString(),
  //       "createdat" : row.createdat.toString(),
  //       "createdby" : row.createdby._id.toString(),
  //       "purchasenotes" : row.purchasenotes.toString(),
  //       "purchasenumber" : row.purchasenumber.toString(),
  //       "purchasetotal" : row.purchasetotal,
  //       "billno" : null,
  //       "challanno" : null,
  //       "productcount" : row.purchaseproducts.length
  //     };
  //   }

  // }
  // if (!row.purchasenotes){
  //   // return {
  //   //   "_id": row._id,
  //   //   "_rev": row._rev,
  //   //   "purchasecustomer" : row.purchasecustomer.name.toString(),
  //   //   "createdat" : row.createdat.toString(),
  //   //   "createdby" : row.createdby._id.toString(),
  //   //   "purchasenotes" : null,
  //   //   "purchasenumber" : row.purchasenumber.toString(),
  //   //   "purchasetotal" : row.purchasetotal,
  //   //   "productcount" : row.purchaseproducts.length
  //   // };
  //   if (row.billno && row.challanno){

  //     return {
  //       "_id": row._id,
  //       "_rev": row._rev,
  //       "purchasecustomer" : row.purchasecustomer.name.toString(),
  //       "createdat" : row.createdat.toString(),
  //       "createdby" : row.createdby._id.toString(),
  //       "purchasenotes" : null,
  //       "purchasenumber" : row.purchasenumber.toString(),
  //       "purchasetotal" : row.purchasetotal,
  //       "billno" : row.billno,
  //       "challanno" : row.challanno,
  //       "productcount" : row.purchaseproducts.length
  //     };
  //   }
  //   if (!row.billno && row.challanno){

  //     return {
  //       "_id": row._id,
  //       "_rev": row._rev,
  //       "purchasecustomer" : row.purchasecustomer.name.toString(),
  //       "createdat" : row.createdat.toString(),
  //       "createdby" : row.createdby._id.toString(),
  //       "purchasenotes" : null,
  //       "purchasenumber" : row.purchasenumber.toString(),
  //       "purchasetotal" : row.purchasetotal,
  //       "billno" : null,
  //       "challanno" : row.challanno,
  //       "productcount" : row.purchaseproducts.length
  //     };

  //   }
  //   if (row.billno && !row.challanno){
  //     return {
  //       "_id": row._id,
  //       "_rev": row._rev,
  //       "purchasecustomer" : row.purchasecustomer.name.toString(),
  //       "createdat" : row.createdat.toString(),
  //       "createdby" : row.createdby._id.toString(),
  //       "purchasenotes" : null,
  //       "purchasenumber" : row.purchasenumber.toString(),
  //       "purchasetotal" : row.purchasetotal,
  //       "billno" : row.billno,
  //       "challanno" : null,
  //       "productcount" : row.purchaseproducts.length
  //     };
  //   }

  //   if (!row.billno && !row.challanno){
  //     return {
  //       "_id": row._id,
  //       "_rev": row._rev,
  //       "purchasecustomer" : row.purchasecustomer.name.toString(),
  //       "createdat" : row.createdat.toString(),
  //       "createdby" : row.createdby._id.toString(),
  //       "purchasenotes" : null,
  //       "purchasenumber" : row.purchasenumber.toString(),
  //       "purchasetotal" : row.purchasetotal,
  //       "billno" : null,
  //       "challanno" : null,
  //       "productcount" : row.purchaseproducts.length
  //     };
  //   }
  // }

  return {
    "_id": row._id,
    "_rev": row._rev,
    "purchasecustomer" : row.purchasecustomer.name.toString(),
    "createdat" : row.createdat.toString(),
    "createdby" : row.createdby._id.toString(),
    "purchasenotes" : row.purchasenotes,
    "purchasenumber" : row.purchasenumber.toString(),
    "purchasetotal" : row.purchasetotal,
    "billno" : row.billno,
    "challanno" : row.challanno,
    "productcount" : row.purchaseproducts.length
  }

}
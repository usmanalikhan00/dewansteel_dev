import { Component, ElementRef, ViewChild, HostListener, Output } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { CustomersService } from '../../services/customers.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
import * as moment from "moment";
import * as _ from "lodash";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {ElectronService} from 'ngx-electron'
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

export interface UserData {
  name ? : string;
  address ? : string;
  phone ? : string;
  opening ? : number;
  closing ? : number;
  status ? : string;
  fax ? : string;
  email ? : string;
  obtype ? : string;
  _id ? : string;
  _rev ? : string;

}

@Component({
    selector: 'customers',
    providers: [LoginService, 
                ProductService, 
                CustomersService, 
                CustomerDebitCreditService, 
                ElectronService],
    templateUrl: 'customers.html',
    styleUrls: ['customers.css']
})

export class customers {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  public customersDB = new PouchDB('steelcustomers');
  public devCustomersDB = new PouchDB('devsteelcustomers');
  public localCustomersDB = new PouchDB('http://localhost:5984/localsteelcustomers');
  public ibmCustomersDB = new PouchDB('https://dewansteel.cloudant.com/dewancustomers', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });
  public cloudantCustomersDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewancustomers', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });

  public customerDebitCreditDB = new PouchDB('steelcustomerdebitcredit');
  public devCustomerDebitCreditDB = new PouchDB('devsteelcustomerdebitcredit');
  public localCustomerDebitCreditDB = new PouchDB('http://localhost:5984/localcustomerdebitcredit');
  public ibmCustomerDebitCreditDB = new PouchDB('https://dewansteel.cloudant.com/dewancustomerdebitcredit', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });
  public cloudantCustomerDebitCreditDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewancustomerdebitcredit', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });




  customers: any = [];
  addCustomerForm: FormGroup;
  dateFilterForm: FormGroup;
  editCustomerForm: FormGroup;
  addCustoemrFlag: boolean = false;
  allCustomers: any= [];
  balanceTypes = ['Credit', 'Debit']
  customerDebitCredits: any = []
  selectedCustomer: any = null
  customerDetailFlag: boolean = false
  editCustomerFlag: boolean = false

  displayedColumns = [
    'name', 
    'address', 
    'phone', 
    'opening', 
    "closing",
    "status",
    'datetime',
    'action'
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  events: string[] = [];
  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);

  toDate: any = null
  fromDate: any = null

  totalDebits: any = null
  totalCredits: any = null
  authUser: any = null
  editCustomerDetails: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _customersService: CustomersService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _electronService: ElectronService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddCustomerForm();
    this._buildEditCustomerForm();
    this._buildDateFilterForm();
    this.loadDataSource()
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: [''],
      email: [''],
      phone: [''],
      fax: [''],
      openingbalance:['', Validators.required],
      obtype:['', Validators.required]
    })
  }

  private _buildEditCustomerForm(){
    this.editCustomerForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: [''],
      email: [''],
      phone: [''],
      fax: [''],
      openingbalance:['', Validators.required],
      obtype:['', Validators.required]
    })
  }



  private _buildDateFilterForm(){
    this.dateFilterForm = this._formBuilder.group({
      fromdate: [''],
      todate: ['']
    })
  }



  ngOnInit(){
    var self = this
    self.syncDb()
    self.syncDebitCredit()
    self.getAllCustomers()
    // this.setDates()
  }




  syncDb(){
    var self = this
    // var sync = PouchDB.sync(self.customersDB, self.ibmCustomersDB, {
    //   live: true,
    //   retry: true
    // }).on('change', function (info) {
    //   console.log("CHANGE EVENT", info)
    //   if (info.direction === 'pull' || info.direction === 'push'){
    //     self.loadDataSource()
    //   }
    //   // handle change
    // }).on('paused', function (err) {
    //   // replication paused (e.g. replication up to date, user went offline)
    //   console.log("PAUSE EVENT FROM CUSTOMER", err)
    // }).on('active', function () {
    //   // replicate resumed (e.g. new changes replicating, user went back online)
    //   console.log("ACTIVE ACTIVE !!")
    // }).on('denied', function (err) {
    //   // a document failed to replicate (e.g. due to permissions)
    //   console.log("DENIED DENIED !!", err)
    // }).on('complete', function (info) {
    //   // handle complete
    //   console.log("COMPLETED !!", info)
    // }).on('error', function (err) {
    //   // handle error
    //   console.log("ERROR ERROR !!", err)
    // });
    var opts = { live: true, retry: true };
    self.customersDB.replicate.from(self.ibmCustomersDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM ONE-WAY CUSTOMER REPLICATION:--", info)
      self.customersDB.sync(self.ibmCustomersDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY CUSTOMER SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          // console.log("----PULL FROM TWO-WAY CUSTOMER SYNC-----")
          self.loadDataSource()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT TWO-WAY SYNC CUSTOMER", err)
      }).on('active', function () {
        console.log("****ACTIVE ACTIVE TWO-WAY SYNC CUSTOMER****")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    });
  }

  cloudantSync(){
    var self = this
    var opts = { live: true, retry: true };
    self.customersDB.replicate.to(self.cloudantCustomersDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY CUSTOMER REPLICATION:--", info)
      // self.productsDB.sync(self.cloudantProductsDB, opts).on('change', function (info) {
      //   console.log("CHANGE EVENT FROM TWO-WAY CLOUDANT PRODUCTS SYNC:--", info)
      //   if (info.direction === 'pull' || info.direction === 'push'){
      //     self.loadSourceData()
      //   }
      // }).on('paused', function (err) {
      //   console.log("PAUSE EVENT FROM TWO-WAY SYNC CLOUDANT PRODUCTS", err)
      // }).on('active', function () {
      //   console.log("ACTIVE ACTIVE FROM TWO-WAY PRODUCTS CLOUDANT SYNC!!")
      // }).on('denied', function (err) {
      //   console.log("DENIED DENIED !!", err)
      // }).on('complete', function (info) {
      //   console.log("COMPLETED !!", info)
      // }).on('error', function (err) {
      //   console.log("ERROR ERROR FROM SYNC CLOUDANT PRODUCTS!!", err)
      // })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT ACCOUNT TYPE !!", err)
    })
    self.customerDebitCreditDB.replicate.to(self.cloudantCustomerDebitCreditDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY CUSTOMER DEBIT/CREDIT REPLICATION:--", info)
      // self.productsDB.sync(self.cloudantProductsDB, opts).on('change', function (info) {
      //   console.log("CHANGE EVENT FROM TWO-WAY CLOUDANT PRODUCTS SYNC:--", info)
      //   if (info.direction === 'pull' || info.direction === 'push'){
      //     self.loadSourceData()
      //   }
      // }).on('paused', function (err) {
      //   console.log("PAUSE EVENT FROM TWO-WAY SYNC CLOUDANT PRODUCTS", err)
      // }).on('active', function () {
      //   console.log("ACTIVE ACTIVE FROM TWO-WAY PRODUCTS CLOUDANT SYNC!!")
      // }).on('denied', function (err) {
      //   console.log("DENIED DENIED !!", err)
      // }).on('complete', function (info) {
      //   console.log("COMPLETED !!", info)
      // }).on('error', function (err) {
      //   console.log("ERROR ERROR FROM SYNC CLOUDANT PRODUCTS!!", err)
      // })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT CUSTOMER DEBIT/CREDIT !!", err)
    })

  }

  syncDebitCredit(){
    var self = this
    // var sync = PouchDB.sync(self.customerDebitCreditDB, self.ibmCustomerDebitCreditDB, {
    //   live: true,
    //   retry: true
    // }).on('change', function (info) {
    //   console.log("CHANGE EVENT", info)
    //   if (info.direction === 'pull'){
    //     self.customerDebitCredit(self.selectedCustomer)
    //   }
    //   // handle change
    // }).on('paused', function (err) {
    //   // replication paused (e.g. replication up to date, user went offline)
    //   console.log("PAUSE EVENT FROM CUSTOMER DEBIT CREDIT", err)
    // }).on('active', function () {
    //   // replicate resumed (e.g. new changes replicating, user went back online)
    //   console.log("ACTIVE ACTIVE !!")
    // }).on('denied', function (err) {
    //   // a document failed to replicate (e.g. due to permissions)
    //   console.log("DENIED DENIED !!", err)
    // }).on('complete', function (info) {
    //   // handle complete
    //   console.log("COMPLETED !!", info)
    // }).on('error', function (err) {
    //   // handle error
    //   console.log("ERROR ERROR !!", err)
    // });
    var opts = { live: true, retry: true };
    self.customerDebitCreditDB.replicate.from(self.ibmCustomerDebitCreditDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM ONE-WAY CUSTOMER DEBIT CREDIT REPLICATION:--", info)
      self.customerDebitCreditDB.sync(self.ibmCustomerDebitCreditDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY CUSTOMER DEBIT CREDIT SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          console.log("----PULL FROM TWO-WAY CUSTOMER DEBIT CREDIT SYNC-----")
          self.customerDebitCredit(self.selectedCustomer)
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT TWO-WAY SYNC CUSTOMER DEBIT CREDIT", err)
      }).on('active', function () {
        console.log("****ACTIVE ACTIVE TWO-WAY SYNC CUSTOMER DEBIT CREDIT****")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    });
  }

  loadDataSource(){
    var self = this;
    self._customersService.allCustomers().then(function(result){
      // console.log("ALL RESULTS FROM CUSTOMER:=====", result);
      const users: UserData[] = [];
      result.rows.map(function(row){
        // row.doc.openingbalance = 0 
        // row.doc.closingbalance = 0 
        users.push(createNewUser(row.doc))
      })
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
    }).catch(function(err){
      console.log(err);
    })

  }

  clearFilters(){
    (<FormGroup>this.dateFilterForm).setValue({'fromdate':'', 'todate':''}, {onlySelf: true})
    this.customerDebitCredit(this.selectedCustomer)
  }

  setDates(){
    // var moment
    // console.log("DATE CHANGES:----", $event.target.value)
    // var inputyear = parseInt(moment($event.target.value,"YYYY/MM/DD").format("YYYY"))
    // var inputmonth = parseInt(moment($event.target.value,"YYYY/MM/DD").format("MM"))
    // var inputday = parseInt(moment($event.target.value,"YYYY/MM/DD").format("DD"))
    // console.log("YEAR AFTER FORMATING INPUT DATE:--\n", "INPUT YEAR\n", inputyear, typeof(inputyear), "INPUT MONTH\n", inputmonth, typeof(inputmonth), "INPUT DAYY\n", inputday, typeof(inputday))
    // var currentyear = parseInt(moment().format("YYYY"))
    // var currentmonth = parseInt(moment().format("MM"))
    // var currentday = parseInt(moment().format("DD"))
    // console.log("STATS OF TODAY:--", currentyear, typeof(currentyear), currentmonth, typeof(currentmonth), currentday, typeof(currentday))
    // this.minDate = new Date(currentyear, currentmonth-1, currentday);

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  refreshData($event){
    console.log('fuck you!!', $event)
    this.loadDataSource()
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    console.log("EVENT ROM DATE CHANGE:----", event.value)
    // this.events.push(`${type}: ${event.value}`);
    if (type === 'input' || type === 'change'){
      this.fromDate = event.value.toISOString()
    }
    if (type === 'input2' || type === 'change2'){
      this.toDate = event.value.toISOString()
    }
    console.log("TO DATE:----\n", this.toDate, "\n", "FROM DATE:----\n", this.fromDate)

    if (this.fromDate < this.toDate){
      console.log("FROM DATE  SMALL")
    }else{
      console.log("FROM DATE LARGER")
    }
    if (this.toDate < this.fromDate){
      console.log("TO DATE  SMALL")
    }else{
      console.log("TO DATE LARGER")
    }

    if (this.toDate != null && this.fromDate != null){

      var self = this
      self._customerDebitCreditService.filterDebitCredits(self.selectedCustomer, self.toDate, self.fromDate).then(function(result){
        // console.log("RESULT FROM FILTER DEBIT CREDIT:--", result)
        self.customerDebitCredits = []
        result.docs.forEach(function(doc){
          self.customerDebitCredits.push(doc)
        })
        self._customerDebitCreditService.backDatedDebitCredit(self.selectedCustomer, self.fromDate).then(function(result){
          // console.log("RESULT FROM BCK DATED 9999++++++++++++ DEBIT CREDIT:--", result)
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }

  }


  clearFilter(){
    this.fromDate = null
    this.toDate = null
  }

  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) {
        row.doc.openingbalance = 0 
        row.doc.closingbalance = 0 
        self.allCustomers.push(row.doc); 
      });
      // console.log("ALL CUSTOMERS:=====", self.allCustomers);
      self.debitCreditTotal(self.allCustomers)
    }).catch(function(err){
      console.log(err);
    })
  }

  editCustomer(row){
    (<FormGroup>this.editCustomerForm).patchValue({'name':row.name,
                                                  'email':row.email,
                                                  'address':row.address,
                                                  'phone':row.phone,
                                                  'fax':row.fax,
                                                  'obtype':row.obtype,
                                                  'openingbalance':row.opening},{onlySelf:true})
    // row.openingbalance = row.opening
    // console.log("CUSTOMER TO EDIT:-------", row)
    this.editCustomerFlag = true
    this.editCustomerDetails = row
  }

  hideEdit(){
    this.editCustomerFlag = false
    this.loadDataSource()
  }

  udpateCustomerDetail(values){
    var self = this;
    if (values.obtype === 'Debit'){
      if (values.openingbalance != 0){
        values.closingbalance = values.openingbalance
        values.status = 'debit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }else if (values.obtype === 'Credit'){
      if (values.openingbalance != 0){
        values.openingbalance = -values.openingbalance
        values.closingbalance = values.openingbalance
        values.status = 'credit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }
    values._id = this.editCustomerDetails._id
    values._rev = this.editCustomerDetails._rev
    // console.log("EDIT CUSTOMER DETAILS:----", values)
    self._customersService.updateCustomerBalance(values).then(function(result){
      // console.log("RESULT AFTER UPDATING CUSTOMER:---", result)
      self.editCustomerForm.reset()
      // self.loadDataSource()
      self.editCustomerFlag = false
    }).catch(function(err){
      console.log(err)
    })
  }


  debitCreditTotal(customers){
    var debit = 0
    var credit = 0
    for (let item of customers){
      if (item.status === "debit"){
        debit = debit + item.closingbalance
      }
      if (item.status === "credit"){
        credit = credit + item.closingbalance
      }
    }
    this.totalDebits = debit
    this.totalCredits = credit
    // console.log("TOTAL DEBITS:--", this.totalDebits, "\n", "TOTAL CREDITS:--", this.totalCredits, "\n");
  }

  addCustomer(values){
    var self = this;
    if (values.obtype === 'Debit'){
      if (values.openingbalance != 0){
        values.closingbalance = values.openingbalance
        values.status = 'debit'
      }else{
        values.closingbalance = 0
        values.status = null
      }
    }else if (values.obtype === 'Credit'){
      if (values.openingbalance != 0){
        values.openingbalance = -values.openingbalance
        values.closingbalance = values.openingbalance
        values.status = 'credit'
      }else{
        values.closingbalance = 0
        values.status = null
      }
    }
    // console.log("ADD CUSTOMER CALLED:--", values);
    self._customersService.addCustomer(values).then(function(result){
      // console.log("Customer ADDED:===", result);
        self.addCustomerForm.reset();
        self.loadDataSource()
        // self.addProductForm.clearValidators();
      // console.log("CHANGE VARIACLE", changes);
    }).catch(function(err){
      console.log(err);
    })
  }

  customerDebitCredit(customer){
    var self = this
    self.selectedCustomer = customer
    // self.syncDebitCredit()
    self._customerDebitCreditService.getDebitCredits(customer).then(function(result){
      self.customerDebitCredits = []
      result.docs.forEach(function(doc){
        self.customerDebitCredits.push(doc)
      })
      // console.log("RESULT AFTER ALL CUSTOMER DEBIT CREDITS:-----", self.customerDebitCredits)
      self.customerDebitCredits = _.orderBy(self.customerDebitCredits, ['_id'], ['asc']);
      self.customerDetailFlag = true
    }).catch(function(err){
      console.log(err)
    })
  }  




  goToPrint(){
    // this._router.navigate(['customerledger', this.selectedCustomer._id])
    this._electronService.ipcRenderer.send('CustomerLedgerPrint', this.selectedCustomer._id)
  }

  goToPrintCustomers(){
    // this._router.navigate(['printcustomers'])
    this._electronService.ipcRenderer.send('CustomersPrint')
  }

  goBack(){
    if (this.authUser._id === 'admin')
      this._router.navigate(['/admindashboard'])
    else if (this.authUser._id === 'deo')
      this._router.navigate(['/deodashboard'])
  }

  hideCuatomerDetail(){
    this.customerDetailFlag = false
    this.loadDataSource()
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }  
}

function createNewUser(row): UserData {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    "_id": row._id,
    "_rev": row._rev,
    "name" : row.name,
    "address" : row.address,
    "phone" : row.phone,
    "opening" : row.openingbalance,
    "closing" : row.closingbalance,
    "status" : row.status,
    "fax" : row.fax,
    "obtype" : row.obtype,
    "email" : row.email
  };


}
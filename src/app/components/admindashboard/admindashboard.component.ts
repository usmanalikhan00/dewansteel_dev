import {Component, ElementRef, ViewChild, HostListener} from '@angular/core';
import {LoginService} from '../../services/login.service'
import {InvoiceService} from '../../services/invoice.service'
import {PurchaseService} from '../../services/purchase.service'
import {ProductService} from '../../services/product.service'
// import { User } from '../../models/model-index'
import { AccountTypesService } from '../../services/accounttypes.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib';
PouchDB.plugin(PouchFind)
import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'admin-dashboard',
    providers: [LoginService,
                InvoiceService,
                PurchaseService,
                ProductService,
                AccountTypesService],
    templateUrl: 'admindashboard.html',
    styleUrls: ['admindashboard.css']
})

export class admindashboard {
  
  public errorMsg = '';
  public userDB = new PouchDB('users');
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  users: any = [];
  allAccounts: any = [];
  allProducts: any = [];
  allInvoices: any = [];
  allPurchases: any = [];
  totalStockValue: any = 0
  totalInvoice: any = 0
  totalPurchases: any = 0

  addProductForm: FormGroup;
  authUser: any = null

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _productService: ProductService, 
              private _accountTypesService: AccountTypesService, 
              private _router: Router) {
    this._buildAddProductForm();

  }

  private _buildAddProductForm(){
    
  }

  ngOnInit(){
    if (window.innerWidth < 886) {
      this.navMode = 'over';
    }
    this.authUser = JSON.parse(localStorage.getItem('user'))
    console.log("AUTH USER:-------", this.authUser)
    this.getAllAccounts()
    this.profitLossAccount()
  }

  getAllAccounts(){
    var self = this
    self._accountTypesService.getPendingAccounts().then(function(result){
      console.log("ALL PENDING ACCOUNTS:--------", result)
      self.allAccounts = []
      result.docs.forEach(function(row){
        // if ()
        let diffLeadtime = moment(row.expirydate).diff(moment().format('YYYY-MM-DD'),'days');
        console.log("DIFF IN LEAD TIME:--", diffLeadtime)
        row.notice = diffLeadtime
        self.allAccounts.push(row)
        console.log("ALL ACCOUNTS AFTER DIFFERENCE WITH CURRENT TIME:--------", self.allAccounts)
      })
    }).catch(function(err) {
      console.log(err)
    })
  }

  profitLossAccount(){
    var self = this
    self._productService.allProducts().then(function(result){
      var stockSum = 0
      result.rows.map(function (row) { 
        self.allProducts.push(row.doc)
        stockSum = stockSum + row.doc.networth 
      });
      self.totalStockValue = stockSum
      console.log("ALL PRODCTS * SUM:--", self.allProducts, self.totalStockValue)
      self._invoiceService.allInvocies().then(function(result){
        var stockSum = 0
        result.rows.map(function (row) { 
          self.allInvoices.push(row.doc); 
          stockSum = stockSum + row.doc.invoicetotal 
        });
        self.totalInvoice = stockSum
        console.log("ALL INVOICES:--", self.allInvoices, self.totalInvoice)
        self._purchaseService.allPurchases().then(function(result){
          var stockSum = 0
          result.rows.map(function (row) { 
            self.allPurchases.push(row.doc); 
            stockSum = stockSum + row.doc.purchasetotal 
          });
          self.totalPurchases = stockSum
          console.log("ALL PURCHASES:--", self.allPurchases, self.totalPurchases)
        }).catch(function(err) {
          console.log(err)
        })
      }).catch(function(err) {
        console.log(err)
      })    
    }).catch(function(err) {
      console.log(err)
    })
  }

  goToAccounts(){
    this._router.navigate(['/cashsettings'])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
  
  logout(){
    this._loginService.logout()
  }  
}
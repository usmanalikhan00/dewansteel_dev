var { app, BrowserWindow, ipcMain } = require('electron');
var url = require('url');
var path = require('path');
var print_win;
let win = null;

process.setMaxListeners(0);

app.on('ready', () => {
 
    win = new BrowserWindow({ width: 1000, height: 600 });
     
    // win.loadURL('http://localhost:4200');

    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
   
    win.on('closed', () => {
        win = null;
    })
 
    // win.webContents.openDevTools();
})
 
app.on('activate', () => {
    if (win == null)
    createWindow()    
})
 
app.on('window-all-closed', () => {
    if (process.platform != 'darwin') {
        app.quit();
    }
})

ipcMain.on('loadPrintPage', (event, arg) => {
    let invoiceId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printsingleinvoice/"+invoiceId+"'");
    });
});

ipcMain.on('InventoryLedgerPrint', (event, arg) => {
    let productId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/inventoryledger/"+productId+"'");
    });
});

ipcMain.on('PurchasePrint', (event, arg) => {
    let purchaseId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printpurchase/"+purchaseId+"'");
    });
});

ipcMain.on('InwardpassPrint', (event, arg) => {
    let inwardpassId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printinwardpass/"+inwardpassId+"'");
    });
});

ipcMain.on('OutwardpassPrint', (event, arg) => {
    let outwardpassId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printoutwardpass/"+outwardpassId+"'");
    });
});

ipcMain.on('CustomerLedgerPrint', (event, arg) => {
    let customerId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/customerledger/"+customerId+"'");
    });
});

ipcMain.on('InvoiceReturnPrint', (event, arg) => {
    let invoicereturnId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printinvoicereturn/"+invoicereturnId+"'");
    });
});

ipcMain.on('PurchaseReturnPrint', (event, arg) => {
    let purchasereturnId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printpurchasereturn/"+purchasereturnId+"'");
    });
});

ipcMain.on('PurchasesPrint', (event, arg) => {
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printpurchases/'");
    });
});

ipcMain.on('InvoicesPrint', (event, arg) => {
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printinvoices/'");
    });
});

ipcMain.on('ProductsPrint', (event, arg) => {
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printproducts/'");
    });
});

ipcMain.on('CustomersPrint', (event, arg) => {
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printcustomers/'");
    });
});



ipcMain.on('PaymentPrint', (event, arg) => {
    let paymentId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printpayment/"+paymentId+"'");
    });
});

ipcMain.on('ReceiptPrint', (event, arg) => {
    let receiptId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/printreceipt/"+receiptId+"'");
    });
});

ipcMain.on('AccountLedgerPrint', (event, arg) => {
    let accountId = arg;
    print_win = new BrowserWindow({ 'auto-hide-menu-bar': true, show: false });
    print_win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
    print_win.webContents.on('did-finish-load', function() {
      print_win.webContents.executeJavaScript("window.location.hash='#/accountsledger/"+accountId+"'");
    });
});






ipcMain.on('Print', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintPayment', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1,
  });
});

ipcMain.on('PrintReceipt', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintPurchase', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1,
  });
});

ipcMain.on('PrintInwardpass', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintOutwardpass', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintInventoryLedger', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});



ipcMain.on('PrintAccountLedger', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});


ipcMain.on('PrintPurchaseReturn', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});


ipcMain.on('PrintInvoiceReturn', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintCustomers', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});




ipcMain.on('PrintInvoices', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});


ipcMain.on('PrintPurchases', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});

ipcMain.on('PrintProducts', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});


ipcMain.on('PrintCustomerLedger', (event, arg) => {
  print_win.webContents.print({
    silent:true, 
    printBackground: true, 
    marginsType: 1
  });
});
